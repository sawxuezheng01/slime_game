var uuid = require('uuid');
var SAT = require('sat');
var config = require('./config');

var MAX_TRIALS = 10;

var COIN_DEFAULT_RADIUS = 10;
var COIN_DEFAULT_VALUE = 1;

var TrapManager = function (options) {
  this.cellData = options.cellData;

  var cellBounds = options.cellBounds;
  this.cellBounds = cellBounds;
  this.cellX = cellBounds.minX;
  this.cellY = cellBounds.minY;
  this.cellWidth = cellBounds.maxX - cellBounds.minX;
  this.cellHeight = cellBounds.maxY - cellBounds.minY;

  this.traps = {};
};

TrapManager.prototype.addTrap = function(pos_x, pos_y, owner){

  var now = Date.now();
  var trap_id = uuid.v4();
  var trap = {
    id: trap_id,
    owner: owner,
    type: 'trap',
    x: pos_x,
    y: pos_y,
    r: config.DEFAULT_TRAP_RANGE,
    set_time: now
  };

  this.traps[trap_id] = trap;
  return trap;
};

TrapManager.prototype.removeTrap = function (trapid){
  if (this.traps[trapid]){
    this.traps[trapid].delete = 1;
    delete this.traps[trapid];  
  } 
};

TrapManager.prototype.isWithinTrapRange = function(pos_x, pos_y, trapid, traprange){

  var trapradius = traprange|| config.DEFAULT_TRAP_RANGE;
  var trap = this.traps[trapid];
  //if the trap exist
  if (trap){
    //construct a circle around the trap
    //check if pos_x and pos_y is within the circle
    //if it is, then it is inside the range
    var trapCircle = new SAT.Circle(new SAT.Vector(trap.x, trap.y), trapradius);
    var playertrapcircle = new SAT.Circle(new SAT.Vector(pos_x,pos_y), trapradius);
    return SAT.testCircleCircle(playertrapcircle, trapCircle);
  } 

  return false;
};

module.exports.TrapManager = TrapManager;