sumomo
======

To run server 
> node server

|FILE NAME|DESCRIPTION|
|-----------|-----------|
|Worker.js| Deals with publishing and exchanges between server and channels
|Cell.js| Deals with physics engine and objects interactions
|config.js| Game settings. You can filter out properties that don't need to be send to the front-end client here


TO-DO
===
Clean up unnecessary code. Reorganize modules.
Testbenches. Exceptions handling.
Port to android and ios.
Documentations.
