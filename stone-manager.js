var uuid = require('uuid');
var SAT = require('sat');
var config = require('./config');

var MAX_TRIALS = 10;

var STONE_DEFAULT_RADIUS = 50;
var STONE_DEFAULT_VALUE = 1;

var StoneManager = function (options) {
  this.cellData = options.cellData;

  var cellBounds = options.cellBounds;
  this.cellBounds = cellBounds;
  this.cellX = cellBounds.minX;
  this.cellY = cellBounds.minY;
  this.cellWidth = cellBounds.maxX - cellBounds.minX;
  this.cellHeight = cellBounds.maxY - cellBounds.minY;

  this.playerNoDropRadius = options.playerNoDropRadius;
  this.stoneMaxCount = options.stoneMaxCount;
  this.stoneDropInterval = options.stoneDropInterval;

  this.stones = {};
  this.stoneCount = 0;
};

StoneManager.prototype.generateRandomAvailablePosition = function (stoneRadius) {
  var stoneDiameter = stoneRadius * 2;
  var circles = [];
  var validCircles = [];

  var players = this.cellData.player;
  
  for (var i in players) {
    var curPlayer = players[i];
    circles.push(new SAT.Circle(new SAT.Vector(curPlayer.x, curPlayer.y), this.playerNoDropRadius));
  }

  for (var i in config.RING_BOUNDARIES){
    var ring = config.RING_BOUNDARIES[i];
    validCircles.push(new SAT.Circle(new SAT.Vector(ring.x, ring.y), ring.r));
  }

  var position = null;

  for (var j = 0; j < MAX_TRIALS; j++) {
    var tempPosition = {
      x: this.cellX + Math.round(Math.random() * (this.cellWidth - stoneDiameter) + stoneRadius),
      y: this.cellY + Math.round(Math.random() * (this.cellHeight - stoneDiameter) + stoneRadius)
    }

    // var tempPosition = generateRandomPosInRing();

    var tempPoint = new SAT.Vector(tempPosition.x, tempPosition.y);

    var validPosition = true;
    for (var k = 0; k < circles.length; k++) {
      if (SAT.pointInCircle(tempPoint, circles[k])) {
        validPosition = false;
        break;
      }
      // if (!SAT.pointInPolygon(tempPoint, box.toPolygon())){
      //   validPosition = false;
      //   break;
      // }
    }

    for (var k = 0; k < validCircles.length; k++) {
      if (!SAT.pointInCircle(tempPoint, validCircles[k])) {
        validPosition = false;
        break;
      }
    }


    if (validPosition) {
      position = tempPosition;
      break;
    }
  }

  return position;
};

// StoneManager.prototype.generateRandomAvailablePosition = function (stoneRadius) {
//   var stoneDiameter = stoneRadius * 2;
//   var circles = [];

//   var players = this.cellData.player;

//   for (var i in players) {
//     var curPlayer = players[i];
//     circles.push(new SAT.Circle(new SAT.Vector(curPlayer.x, curPlayer.y), this.playerNoDropRadius));
//   }

//   var position = null;

//   for (var j = 0; j < MAX_TRIALS; j++) {
//     var tempPosition = {
//       x: this.cellX + Math.round(Math.random() * (this.cellWidth - stoneDiameter) + stoneRadius),
//       y: this.cellY + Math.round(Math.random() * (this.cellHeight - stoneDiameter) + stoneRadius)
//     }

//     var tempPoint = new SAT.Vector(tempPosition.x, tempPosition.y);

//     var validPosition = true;
//     for (var k = 0; k < circles.length; k++) {
//       if (SAT.pointInCircle(tempPoint, circles[k])) {
//         validPosition = false;
//         break;
//       }
//     }
//     if (validPosition) {
//       position = tempPosition;
//       break;
//     }
//   }
//   return position;
// };


StoneManager.prototype.addStone = function (value, subtype, radius) {
  radius = radius || STONE_DEFAULT_RADIUS;
  var stoneId = uuid.v4();
  var validPosition = this.generateRandomAvailablePosition(radius);
  if (validPosition) {
    var stone = {
      id: stoneId,
      type: 'stone',
      t: subtype || 1,
      v: value || STONE_DEFAULT_VALUE,
      r: radius,
      x: validPosition.x,
      y: validPosition.y
    };
    this.stones[stoneId] = stone;
    this.stoneCount++;
    return stone;
  }
  return null;
};

StoneManager.prototype.removeStone = function (stoneId) {
  var stone = this.stones[stoneId];
  if (stone) {
    stone.delete = 1;
    delete this.stones[stoneId];
    this.stoneCount--;
  }
};

StoneManager.prototype.doesPlayerTouchStone = function (stoneId, player) {
  var stone = this.stones[stoneId];
  if (!stone) {
    return false;
  }
  var playerCircle = new SAT.Circle(new SAT.Vector(player.x, player.y), Math.ceil(player.width / 2));
  var stoneCircle = new SAT.Circle(new SAT.Vector(stone.x, stone.y), stone.r);
  return SAT.testCircleCircle(playerCircle, stoneCircle);
};

// function generateRandomPosInRing(){
//   var t = 2*Math.PI*Math.random();
//   var u = Math.random()*Math.random();
//   var r;

//   if (u > 1) {
//     r = 2 - u;
//   } else {
//     r = u;
//   }

//   //get a random circle from the config
//   var rand_circle = config.RING_BOUNDARIES[Math.floor(Math.random()*config.RING_BOUNDARIES.length)];
//   var offset_x = rand_circle.x;
//   var offset_y = rand_circle.y;
//   var radius_mult = rand_circle.r;

//   var rand_x = offset_x + r*radius_mult*Math.cos(t);
//   var rand_y = offset_y + r*radius_mult*Math.sin(t);

//   return {x: rand_x, y: rand_y};
// };


module.exports.StoneManager = StoneManager;
