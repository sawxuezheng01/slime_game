/*
  Note that the main run() loop will be executed once per frame as specified by WORLD_UPDATE_INTERVAL in worker.js.
  Behind the scenes, the engine just keeps on building up a cellData tree of all different
  state objects that are present within our current grid cell.
  The tree is a simple JSON object and needs to be in the format:

    {
      // player is a state type.
      player: {
        // ...
      },
      // You can add other custom state types.
      someType: {
        // Use the id as the key (in the place of someId).
        // It is recommended that you use a random uuid as the state id. See https://www.npmjs.com/package/uuid
        someId: {
          // All properties listed here are required.
          // You can add additional ones.
          id: someId,
          type: someType,
          x: someXCoordinate,
          y: someYCoordinate,
        },
        anotherId: {
          // ...
        }
      }
    }

  You can add new type subtrees, new states and new properties to the cellData
  as you like. So long as you follow the structure above, the items will show
  up on the front end in the relevant cell in our world (see the handleCellData function in index.html).

  Adding new items to the cell is easy.
  For example, to add a new coin to the cell, you just need to add a state object to the coin subtree (E.g):
  cellData.coin[coin.id] = coin;

  See how coinManager.addCoin is used below (and how it's implemented) for more details.

  Note that states which are close to our current cell (based on WORLD_CELL_OVERLAP_DISTANCE)
  but not exactly inside it will still be visible within this cell (they will have an additional
  'external' property set to true).

  External states should not be modified because they belong to a different cell and the change will be ignored.
*/

var _ = require('lodash');
var rbush = require('rbush');
var SAT = require('sat');
var config = require('./config');
var CoinManager = require('./coin-manager').CoinManager;
var p2 = require('p2'); //physics engine
var Body = require('p2').Body; //physics engine
var node_util = require('util'); //to weed out circular objects before change to json
var StateManager = require('./state-manager').StateManager;
var TrapManager = require('./trap-manager').TrapManager;

// This controller will be instantiated once for each
// cell in our world grid.

var CellController = function (options, util, material, rankmanager, botmanager) {
  var self = this;

  //setup a world with no gravity and passes into cellController
  this.world = new p2.World({
    gravity:[0, 0]
  });

            // // Create top plane
            // var planetop = new p2.Body({
            //     position : [0,0],
            // });
            // planetop.addShape(new p2.Plane());
            // this.world.addBody(planetop);

            // // Create bottom plane
            // var planebottom = new p2.Body({
            //     position : [0,config.WORLD_HEIGHT],
            // });
            // planebottom.addShape(new p2.Plane());
            // this.world.addBody(planebottom);
            // // Left plane
            // var planeLeft = new p2.Body({
            //     angle: -Math.PI/2,
            //     position: [0, 0]
            // });
            // planeLeft.addShape(new p2.Plane());
            // this.world.addBody(planeLeft);
            // // Right plane
            // var planeRight = new p2.Body({
            //     angle: Math.PI/2,
            //     position: [config.WORLD_WIDTH, 0]
            // });
            // planeRight.addShape(new p2.Plane());
            // this.world.addBody(planeRight);


  this.world.addContactMaterial(new p2.ContactMaterial(material, material, {
                restitution: config.MATERIAL_RESTITUTION
    }));

  this.material = material;
  this.rankmanager = rankmanager;
  this.cellIndex = options.cellIndex;
  this.options = options;
  this.util = util;  

  // You can use the exchange object to publish data to global channels which you
  // can watch on the front end (in index.html).
  // The API for the exchange object is here: http://socketcluster.io/#!/docs/api-exchange
  // To receive channel data on the front end, you can read about the subscribe and watch
  // functions here: http://socketcluster.io/#!/docs/basic-usage
  this.exchange = options.worker.exchange;

  this.worldColCount = Math.ceil(config.WORLD_WIDTH / config.WORLD_CELL_WIDTH);
  this.worldRowCount = Math.ceil(config.WORLD_HEIGHT / config.WORLD_CELL_HEIGHT);
  this.worldCellCount = this.worldColCount * this.worldRowCount;
  this.workerCount = options.worker.options.workers;

  this.coinMaxCount = Math.round(config.COIN_MAX_COUNT / this.worldCellCount);
  this.coinDropInterval = config.COIN_DROP_INTERVAL * this.worldCellCount;
  
  this.botManager = botmanager;
 

  var cellData = options.cellData;

  // //--------Bot stuff---------//
  // this.botManager = new BotManager({
  //   worldWidth: config.WORLD_WIDTH,
  //   worldHeight: config.WORLD_HEIGHT,
  //   botDefaultDiameter: config.BOT_DEFAULT_DIAMETER,
  //   botMoveSpeed: config.BOT_MOVE_SPEED,
  //   botMass: config.BOT_MASS,
  //   botChangeDirectionProbability: config.BOT_CHANGE_DIRECTION_PROBABILITY,
  //   botbotSuperSizeProbabilty: config.BOT_SUPERSIZE_PROBABILITY
  // });

  if (!cellData.player) {
    cellData.player = {};
  }


 this.trapManager = new TrapManager({
    cellData: options.cellData,
    cellBounds: options.cellBounds
 });


  //----Start Coin Stuff ---//
  this.coinManager = new CoinManager({
    cellData: options.cellData,
    cellBounds: options.cellBounds,
    playerNoDropRadius: config.COIN_PLAYER_NO_DROP_RADIUS,
    coinMaxCount: this.coinMaxCount,
    coinDropInterval: this.coinDropInterval
  });

  this.lastCoinDrop = 0;

  config.COIN_TYPES.sort(function (a, b) {
    if (a.probability < b.probability) {
      return -1;
    }
    if (a.probability > b.probability) {
      return 1;
    }
    return 0;
  });

  this.coinTypes = [];
  var probRangeStart = 0;
  config.COIN_TYPES.forEach(function (coinType) {
    var coinTypeClone = _.cloneDeep(coinType);
    coinTypeClone.prob = probRangeStart;
    self.coinTypes.push(coinTypeClone);
    probRangeStart += coinType.probability;
  });

  this.playerCompareFn = function (a, b) {
    if (a.id < b.id) {
      return -1;
    }
    if (a.id > b.id) {
      return 1;
    }
    return 0;
  };
};





//--START HERE TO CHANGE THE MECHANICS OF THE GAME!! --//
/*
  The main run loop for our cell controller.
*/
CellController.prototype.run = function (cellData) {
  self = this;
  if (!cellData.player) {
    cellData.player = {};
  }

  if (!cellData.coin) {
    cellData.coin = {};
  }

  if (!cellData.trap){
    cellData.trap = {};
  }

  if (!this.playerBodies)
  {
    this.playerBodies = {};
  }

  if (!this.coinBodies)
  {
    this.coinBodies = {};
  }  

  var players = cellData.player;
  var coins = cellData.coin;
  var traps = cellData.trap;

  // Sorting is important to achieve consistency across cells.
  var playerIds = Object.keys(players).sort(this.playerCompareFn);
  var coinIds = Object.keys(coins).sort(this.playerCompareFn);
  var trapIds = Object.keys(traps).sort(this.playerCompareFn);

  //remove the previous listener if it exist
  if (typeof this.listener == 'function'){
    this.world.off('impact', this.listener);
  }

  //listen for any collision
  this.listener = function(event){
    
    var bodyA = event.bodyA;
    var bodyB = event.bodyB;

    var id_a = event.bodyA.playerid || event.bodyA.coinid;
    var id_b = event.bodyB.playerid || event.bodyB.coinid;

    var tagged_time = Date.now();

    //p p , p c , c p , c c 
    //if both are players
    if (event.bodyA.playerid && event.bodyB.playerid){
      //tag the player with their id
      players[id_a].lasthitby = id_b;
      players[id_b].lasthitby = id_a;
      
      //ignore if one of the player is in dig form
      if (players[id_a].dig || players[id_b].dig){
        return;
      }

      players[id_a].tagged_time = tagged_time;
      players[id_b].tagged_time = tagged_time;

      self.util.groupStates([players[id_a], players[id_b]]);

    } //if both are coins 
    else if (event.bodyA.coinid && event.bodyB.coinid){4
      self.util.groupStates([coins[id_a], coins[id_b]]);
    } else {
        if (event.bodyA.playerid){
          if (players[id_a].dig){
            return;
          }

          players[id_a].lasthitby = id_b;
          coins[id_b].lasthitby = id_a;

          players[id_a].tagged_time = tagged_time;
          coins[id_b].tagged_time = tagged_time;

          self.util.groupStates([players[id_a], coins[id_b]]);
        } else {
          if (players[id_b].dig){
            return;
          }

          players[id_b].lasthitby = id_a;
          coins[id_a].lasthitby = id_b;

          players[id_b].tagged_time = tagged_time;
          coins[id_a].tagged_time = tagged_time;

          self.util.groupStates([coins[id_a], players[id_b]]);
        }
    }



    var mass_average = (bodyA.mass + bodyB.mass)/2;
    var force_angle = Math.atan2( bodyA.position[0] - bodyB.position[0], bodyA.position[1] - bodyB.position[1]);
    var applied_accel_x = Math.round(Math.cos(force_angle) * config.COLLISION_MULTIPLIER * mass_average);
    var applied_accel_y = Math.round(Math.sin(force_angle) * config.COLLISION_MULTIPLIER * mass_average);

    bodyA.force = [applied_accel_x,applied_accel_y];
    bodyB.force = [-applied_accel_x,-applied_accel_y];
}
  // Add event listener (for new data) to group states together during collision
  this.world.on('impact', this.listener);

  // console.log("cell start");
  // console.log("spawning bots");
  this.spawnBots(players);

  //Associate each player with a body
  // console.log("associating body");
  this.associateBody(this.material, playerIds, players, this.playerBodies);
  this.associateCoinBody(this.material, coinIds, coins, this.coinBodies);

  //check if player overlaps with coins and stuff
  // console.log("finding playeroverlaps");
  this.findPlayerOverlaps(playerIds, players, traps);
  //console.log("dropping coins");
  this.dropCoins(coins); 
  //console.log("generating bot ops");
  this.generateBotOps(playerIds, players);


  //apply operations on player (ie pick up coins etc, apply forces, status effect)
  // console.log("applying playerops");
  this.applyPlayerOps(playerIds, players, coins, this.playerBodies, traps);
  this.world.step(config.SIMULATION_STEP);

  // console.log("checkingplayersurvived");
  this.checkPlayerOnSurvivalRing(playerIds, players, this.playerBodies);
  this.checkCoinOnSurvivalRing(coinIds, coins, this.coinBodies, players, this.playerBodies);
  this.clearTrapsTimeout(trapIds, traps);

  // console.log("transfering body");
  this.transferBodytoPlayer(playerIds, players, this.playerBodies);
  this.transferBodytoPlayer(coinIds, coins, this.coinBodies);

  // console.log("end..");
};


//Get the simulated data from body to the player object
CellController.prototype.transferBodytoCoin = function(coinIds, coins, coinBodies){
    var self = this;

  coinIds.forEach(function (coinid) {

    var coin = coins[coinid];
    var body = coinBodies[coinid];

    if (body){
      coin.x = body.position[0];
      coin.y = body.position[1];
      coin.velocity = body.velocity; 
    }

    var now = Date.now();

    //clears tag after fixed interval
    if (coin.lasthitby && (now - coin.tagged_time > config.COLLIDE_TAG_INTERVAL)){
      delete coin.lasthitby;
    }

  });
}

//Get the simulated data from body to the player object
CellController.prototype.transferBodytoPlayer = function(playerIds, players, playerBodies){
    var self = this;

  playerIds.forEach(function (playerId) {

    var player = players[playerId];
    var body = playerBodies[playerId];

    if (body){
      player.x = body.position[0];
      player.y = body.position[1];
      player.velocity = body.velocity;
      player.radius = body.boundingRadius;
      player.mass = body.mass;
      player.collision_en =  body.collisionResponse;

      self.rankmanager.updatePlayerScore(playerId, Math.round(player.score), player.name);

      //scales the player's maxAccleration with mass
      player.maxAcceleration = player.mass * config.PLAYER_MAX_ACCELERATION_SCALING + config.PLAYER_DEFAULT_MAX_ACCELERATION;
      player.durscaling = player.maxAcceleration/config.PLAYER_DEFAULT_MAX_ACCELERATION;

      //Resolves player's direction

      var angle = Math.atan2(body.velocity[1],body.velocity[0]);
      if (angle > -Math.PI/4 && angle < Math.PI/4){
        player.direction = 'right';
      }
      else if (angle > Math.PI/4 && angle < Math.PI*3/4 ){
        player.direction = 'down';
      }
      else if ((angle > Math.PI*3/4 && angle < Math.PI) || (angle < -Math.PI*3/4 && angle > -Math.PI) ){
        player.direction = 'left';
      }
      else if (angle < -Math.PI/4 && angle > -Math.PI*3/4 ){
        player.direction = 'up';
      }      
    }

    var now = Date.now();

    //clears tag after fixed interval
    if (player.lasthitby && (now - player.tagged_time > config.COLLIDE_TAG_INTERVAL)){
      delete player.lasthitby;
    }

  });
}

//Associate each player with a body and add it to world
CellController.prototype.associateBody = function(material, playerIds, players, playerBodies){
    self = this;
    //remove uneeded plyerBodies
    var keys = Object.keys(playerBodies);
    keys.forEach(function(key){
      //if the player is not in cell anymore, delete the dictionary pairs, remove it from world object
      if (!players[key]){
        self.world.removeBody(playerBodies[key]);
        delete playerBodies[key];
      }
    });

    //Associate body for player
    playerIds.forEach(function(playerid){
      var player = players[playerid];
      var body = playerBodies[playerid];
      //associate a body if is alive and has no body 
      //else if it is alive and already has a body just update 
      //else just ignore
      if(!body && player.alive){

        //P2 Physics Engine
        //create a circle shape
        var circleShape = new p2.Circle({ radius: Math.round(player.radius)});
        // Create an empty dynamic body 
        var body = new p2.Body({
            mass: player.mass,
            position: [player.x, player.y],
            velocity: player.velocity,
            damping: config.PLAYER_DEFAULT_DAMPING,
            material: material,
            collisionResponse: player.collision_en
        });

        body.addShape(circleShape);
        body.playerid = playerid;

        // player.maxAcceleration = config.PLAYER_DEFAULT_MAX_ACCELERATION;
        playerBodies[playerid] = body;

        self.world.addBody(body);
      } else if (body && player.alive) {
        var shape = body.shapes[0];

        body.mass = player.mass;
        body.position = [player.x, player.y];
        body.velocity = player.velocity;
        shape.radius = player.radius;
        body.boundingRadius = player.radius;
        body.collisionResponse = player.collision_en;

        shape.updateBoundingRadius();
        body.updateBoundingRadius();
        body.updateMassProperties();
      }
    });
}


//Associate each coin with a body and add it to world
CellController.prototype.associateCoinBody = function(material, coinIds, coins, coinBodies){
    self = this;
    //remove uneeded coinBodies
    var keys = Object.keys(coinBodies);
    keys.forEach(function(key){
      //if the coin is not in cell anymore, delete the dictionary pairs, remove it from world object
      if (!coins[key]){
        self.world.removeBody(coinBodies[key]);
        delete self.coinManager.coins[key];
        self.coinManager.coinCount--;
        delete coinBodies[key];
      }
    });

    //Associate body for coin
    coinIds.forEach(function(coinid){
      var coin = coins[coinid];
      var body = coinBodies[coinid];
      //associate a body if is not to be removed and has no body 
      //else if it already has a body and not to be remove just update 
      //else it needs to be removed
      if(!body && !coin.remove){

        //P2 Physics Engine
        //create a circle shape
        var circleShape = new p2.Circle({ radius: Math.round(coin.r)});
        // Create an empty dynamic body 
        var body = new p2.Body({
            mass: coin.v,
            position: [coin.x, coin.y],
            velocity: coin.velocity,
            damping: config.STONE_DEFAULT_DAMPING,
            material: material,
        });

        body.addShape(circleShape);
        body.coinid = coinid;

        // self.coinManager.coins[coinid] = coin;
        // self.coinManager.coinCount++;
        // player.maxAcceleration = config.PLAYER_DEFAULT_MAX_ACCELERATION;
        coinBodies[coinid] = body;
        // self.coinManager.coins[coinid] = coin;
        self.world.addBody(body);
      } else if (body && !coin.remove) {
        var shape = body.shapes[0];

        body.mass = coin.mass;
        body.position = [coin.x, coin.y];
        body.velocity = coin.velocity;
        body.updateMassProperties();
      } else {
        coin.delete = 1;
      }
    });
}


CellController.prototype.dropCoins = function (coins) {
  var now = Date.now();

  if (now - this.lastCoinDrop >= this.coinManager.coinDropInterval &&
    this.coinManager.coinCount < this.coinManager.coinMaxCount) {

    this.lastCoinDrop = now;

    var rand = Math.random();
    var chosenCoinType;

    var numTypes = this.coinTypes.length;
    for (var i = numTypes - 1; i >= 0; i--) {
      var curCoinType = this.coinTypes[i];
      if (rand >= curCoinType.prob) {
        chosenCoinType = curCoinType;
        break;
      }
    }

    if (!chosenCoinType) {
      throw new Error('There is something wrong with the coin probability distribution. ' +
        'Check that probabilities add up to 1 in COIN_TYPES config option.');
    }

    var coin = this.coinManager.addCoin(chosenCoinType.value, chosenCoinType.type, chosenCoinType.radius);
    if (coin) {
      coins[coin.id] = coin;
    }
  }
};

//Don't Need this
CellController.prototype.generateBotOps = function (playerIds, players, coins) {
  var self = this;

  playerIds.forEach(function (playerId) {
    var player = players[playerId];
    // States which are external are managed by a different cell, therefore changes made to these
    // states are not saved unless they are grouped with one or more internal states from the current cell.
    // See util.groupStates() method near the bottom of this file for details.

    if (player.subtype == 'bot' && !player.external) {
      if (!player.repeatOp){
        player.repeatOp = {};
      }

      if (Math.random() <= player.changeDirProb){
        //generate a random angle
        //-PI to PI
        var rand_angle = 2*Math.PI*Math.random()-Math.PI;
        //generate a random duration
        var rand_dur = Math.floor(config.BOT_MAX_CHARGE_TIME*Math.random());

        player.repeatOp.c = 1;
        player.repeatOp.c_dur = rand_dur;
        player.repeatOp.c_angle = rand_angle;
      }

      if (Math.random() <= player.supersizeProb){
        player.repeatOp.b = 1;
      }

      if (Object.keys(player.repeatOp).length > 0){
        //console.log("applying bot operations")
        player.op = player.repeatOp;
      }


      //Get the bot's radius and check if it is on the world's edge
      // var radius = Math.round(player.diam / 2);
      // var isBotOnEdge = player.x <= radius || player.x >= config.WORLD_WIDTH - radius ||
      //   player.y <= radius || player.y >= config.WORLD_HEIGHT - radius;
      // if (Math.random() <= player.changeDirProb || isBotOnEdge) {
      //   var randIndex = Math.floor(Math.random() * self.botMoves.length);
      //   player.repeatOp = self.botMoves[randIndex];
      // }
      // if (player.repeatOp) {
      //   player.op = player.repeatOp;
      // }
    }
  });
};


//check if player is inside the allowed zone if not they ded
CellController.prototype.checkPlayerOnSurvivalRing = function(playerIds, players, playerBodies){
  self = this;

  playerIds.forEach(function(playerid){

    //initialize to true if player is not out of bound for one of the circles then he is not out of bound
    var playeroutofbound = true;
    var outofbound;
    
    player = players[playerid];
    playerBody = playerBodies[playerid];

    if (!player.alive){
      return;
    }
    
    config.RING_BOUNDARIES.forEach(function(circle){


      if (Math.pow((playerBody.position[0] - circle.x),2) + Math.pow((playerBody.position[1] - circle.y),2) > Math.pow(circle.r,2))
      {
           outofbound = true;
      } else {
        outofbound = false;
      }

      //player is out of bound if all the conditions are true
      playeroutofbound = playeroutofbound && outofbound;
    });

    var enoughcrossspeed = false;
    if (p2.vec2.length(playerBody.velocity) > config.CROSS_SPEED){
      enoughcrossspeed = true;
    }

    if (playeroutofbound && !enoughcrossspeed){

        //increase the mass and size of the player's body who killed this guy
        if (player.lasthitby){
          var otherPlayer = players[player.lasthitby];
          var otherPlayerBody = playerBodies[player.lasthitby];

          if (otherPlayerBody){
            otherPlayer.score = otherPlayer.score + Math.round(player.score * config.KILL_SCORE_MULTIPLIER);
            self.applyScoreToRadiusMass(otherPlayer, otherPlayerBody, otherPlayer.score);
          }
        }


        //We can't directly delete the player state internally but will be able to disconnect client externally 
        //through player.alive which will trigger the disconnect event      
        self.world.removeBody(playerBodies[playerid]); //remove the body from the world
        delete playerBodies[playerid]; //delete the object
        //console.log("Removing" + player.name); 
        player.alive = 0;

        //also remember to remove player from ranking
        self.rankmanager.removePlayerFromRanking(player);

        // console.log(player);
        if (player.subtype == 'bot'){
          self.botManager.removeBot(player);

        }
    }
  });
}


//check if player is inside the allowed zone if not they ded
CellController.prototype.checkCoinOnSurvivalRing = function(coinIds, coins, coinBodies, players, playerBodies){
  self = this;

  coinIds.forEach(function(coinid){

    //initialize to true if player is not out of bound for one of the circles then he is not out of bound
    var coinoutofbound = true;
    var outofbound;
    
    coin = coins[coinid];
    coinBody = coinBodies[coinid];

    config.RING_BOUNDARIES.forEach(function(circle){


      if (Math.pow((coinBody.position[0] - circle.x),2) + Math.pow((coinBody.position[1] - circle.y),2) > Math.pow(circle.r,2))
      {
           outofbound = true;
      } else {
        outofbound = false;
      }

      //player is out of bound if all the conditions are true
      coinoutofbound = coinoutofbound && outofbound;
    });

    var enoughcrossspeed = false;
    if (p2.vec2.length(coinBody.velocity) > config.CROSS_SPEED){
      enoughcrossspeed = true;
    }

    if (coinoutofbound && !enoughcrossspeed){

        // increase the mass and size of the player's body who killed this guy
        if (coin.lasthitby){
          // console.log("coin.lasthiby")
          var otherPlayer = players[coin.lasthitby];
          var otherPlayerBody = playerBodies[coin.lasthitby];

          if (otherPlayerBody){
            otherPlayer.score = otherPlayer.score + Math.round(coin.r * config.KILL_SCORE_MULTIPLIER);
            self.applyScoreToRadiusMass(otherPlayer, otherPlayerBody, otherPlayer.score);
          }
        }
        coin.remove = 1;
    }
  });
}



CellController.prototype.spawnBots = function(players){  
  if (Object.keys(this.botManager.botList).length < this.botManager.maxBotCount){
    var bot = this.botManager.addBot(null, players);
    if (bot != null) { 
      players[bot.id] = bot;
      this.rankmanager.addPlayerToRanking(players[bot.id]);
    }
  }
} 


CellController.prototype.applyScoreToRadiusMass = function(player, playerbody, score){

  var applyscore = score || player.score;
  var playernewradius = Math.round(applyscore * config.SCORE_RADIUS_MULTIPLIER);
  var playernewmass = Math.round(applyscore * config.SCORE_MASS_MULTIPLIER);
  
  if (playernewradius <= config.MAX_PLAYER_RADIUS){
    this.applyPlayerRadiusToBody(player, playerbody, playernewradius);
  } else {
    playernewradius = config.MAX_PLAYER_RADIUS;
    this.applyPlayerRadiusToBody(player, playerbody, playernewradius);
  }

  
  this.applyPlayerMassToBody(player, playerbody, playernewmass);
}


//Keep player inside the grid of the game or else they will go out side of the range to be tracked.
CellController.prototype.keepPlayerOnGrid = function (player, body) {

  var radius = 50;

  var leftX = body.position[0] - radius;
  var rightX = body.position[0] + radius;
  var topY = body.position[1] - radius;
  var bottomY = body.position[1] + radius;

  if (leftX < 0) {
    body.position[0] = radius;
    body.velocity[0] = 0;
  } else if (rightX > config.WORLD_WIDTH) {
    body.position[0] = config.WORLD_WIDTH - radius;
    body.velocity[0] = 0;
  }
  if (topY < 0) {
    body.position[1] = radius;
    body.velocity[1] = 0;
  } else if (bottomY > config.WORLD_HEIGHT) {
    body.position[1] = config.WORLD_HEIGHT - radius;
    body.velocity[1] = 0;
  }
};

CellController.prototype.checkTrapsOverlap = function(playerids, players, trapids, traps){

}

//Apply Operations received from client onto the player
CellController.prototype.applyPlayerOps = function (playerIds, players, coins, playerBodies, traps) {
  var self = this;

  playerIds.forEach(function (playerId) {
    var player = players[playerId];
    var body = playerBodies[playerId];

    //return if no body is associated
    if (!body){
      return;
    }

    var now = Date.now(); 
    //apply status effects
    if ( now - player.snared_time < config.DEFAULT_SNARED_DURATION && player.snared){
      return;
    } else {
      player.snared = false;
    }

    //update trap count if after a certain duration
    if (now - player.snare_charge_timer > config.TRAP_CHARGE_DURATION && player.trapCount < config.PLAYER_MAX_TRAPCOUNT){
      player.trapCount++;
      player.snare_charge_timer = now;
    }


    var playerOp = player.op;
    var moveSpeed;
    if (player.subtype == 'bot') {
      moveSpeed = player.speed;
    } else {
      moveSpeed = config.PLAYER_DEFAULT_MOVE_SPEED;
    }

    var angle;

    if (playerOp) { 
      if (playerOp.angle){
        angle = playerOp.angle;
      } else {
        angle = player.lastangle;
      }

      if (playerOp.c){
          var now = Date.now();

          if (now - player.lastapplycharge >= config.PLAYER_CHARGE_COOLDOWN_INTERVAL){
            var angle = playerOp.c_angle;
            var dur = playerOp.c_dur;
            
            var acceleration = dur*moveSpeed*player.durscaling;
            var applied_accel_x;
            var applied_accel_y;

            //limits the maximum force applied
            if (acceleration < player.maxAcceleration)
              {
                applied_accel_x = acceleration*Math.cos(angle);
                applied_accel_y = acceleration*Math.sin(angle);
              }
              else{
                applied_accel_x = player.maxAcceleration*Math.cos(angle);
                applied_accel_y = player.maxAcceleration*Math.sin(angle);
              }

            //applies force to body. Note that force resets after each timestep simulation
            if (!player.supersize)
              {
                body.force = [body.force[0] + applied_accel_x, body.force[1] + applied_accel_y];
                player.lastapplycharge = now;
              }
          }
       }

      //increase mass temperarily operation
      if (playerOp.b){

        // var now = Date.now();
        // //apply break if cooldown is not violated
        // if (now - player.lastapplybreak >= config.PLAYER_BREAK_COOLDOWN_INTERVAL){
        //   body.velocity = [0,0];
        //   player.lastapplybreak = now;
        // }
        var now = Date.now();
        if (now - player.lastapplybreak >= config.PLAYER_BREAK_COOLDOWN_INTERVAL && !player.supersize){

          var supersize_radius = player.radius * config.SUPERSIZE_RADIUS_MULTIPLIER;
          var supersize_mass = body.mass * config.SUPERSIZE_MASS_MULTIPLIER;
          self.applyPlayerRadiusToBody(player, body, supersize_radius);
          self.applyPlayerMassToBody(player, body, supersize_mass);

          body.damping = config.SUPERSIZE_DAMPING;


          player.lastapplybreak = now;
          player.supersize = true;
        }
      }

      //invincible 
      if (playerOp.d){
        var now = Date.now();
        if (now - player.lastapplydig >= config.PLAYER_DIG_COOLDOWN_INTERVAL && !player.dig){
          body.collisionResponse = false;
          player.lastapplydig = now;
          player.dig = true;
        }        
      }

      //teleport
      if (playerOp.t){
        var now = Date.now();
        if (now - player.lastapplytele >= config.PLAYER_TELE_COOLDOWN_INTERVAL){
          var delta_x;
          var delta_y;

          if (player.lastangle){
            delta_x = player.radius * 10 * Math.cos(player.lastangle);
            delta_y = player.radius * 10 * Math.sin(player.lastangle);
          } else {
            var ran_a = Math.random() * 2 * Math.PI - Math.PI;
            delta_x = player.radius * 10 * Math.cos(player.lastangle);
            delta_y = player.radius * 10 * Math.sin(player.lastangle);
          }

          // console.log("teleporting")
          body.position[0] += delta_x;
          body.position[1] += delta_y;
          player.lastapplytele = now;
          player.tele = true;
        }               
      }

      //lay down snare trap
      if (playerOp.s){
        var now = Date.now();
        if (now - player.lastapplytrap >= config.PLAYER_TRAP_COOLDOWN_INTERVAL){
          if (player.trapCount > 0){
            
            var trap = self.trapManager.addTrap(player.x, player.y, player.id);
            traps[trap.id] = trap;
            
            player.trapCount--;
            player.lastapplytrap = now;
            player.snare_charge_timer = now;
          }
        }
      }

    } else {
      if (player.lastangle){
        angle = player.lastangle;
      } else{
        angle = Math.atan2(body.velocity[1],body.velocity[0]);
      }
    }

    //Apply constant force depending on the angle that was given
    var constant_f = config.CONSTANT_FORCE_APPLIED;
    body.force = [body.force[0] + constant_f*Math.cos(angle), body.force[1] + constant_f*Math.sin(angle)];
    player.lastangle = angle;

    //Return to small size after cooldown interval
    if (now - player.lastapplybreak >= config.PLAYER_DIG_COOLDOWN_INTERVAL && player.supersize){

      var shrink_radius = player.radius/config.SUPERSIZE_RADIUS_MULTIPLIER;
      var shrink_mass = body.mass/config.SUPERSIZE_MASS_MULTIPLIER;
      self.applyPlayerRadiusToBody(player, body, shrink_radius);
      self.applyPlayerMassToBody(player, body, shrink_mass);

      body.damping = config.PLAYER_DEFAULT_DAMPING;
      player.supersize = false;

    }    

    if (now - player.lastapplydig >= config.PLAYER_DIG_COOLDOWN_INTERVAL && player.dig){
      body.collisionResponse = true;
      player.dig = false;
    }      

    // if (player.coinOverlaps) {
    //   player.coinOverlaps.forEach(function (coin) {
    //     if (self.testCircleCollision(player, coin).collided) {
    //       //increase the body mass
    //       player.score += coin.v;
    //       self.applyScoreToRadiusMass(player,body,player.score);
    //       self.coinManager.removeCoin(coin.id);
    //     }
    //   });
    //   delete player.coinOverlaps;
    // }

    //if trapped
    if (player.trapOverlaps) {
      player.trapOverlaps.forEach(function (trap) {
        
        if (self.trapManager.isWithinTrapRange(player.x, player.y, trap.id) && trap.owner != player.id) {
          
          //inflict snare status
          body.velocity = [0,0]
          body.position = [trap.x, trap.y];

          var now = Date.now();
          player.snared = true;
          player.snared_time = now;

          //remove trap
          self.trapManager.removeTrap(trap.id);
        }
      });
      delete player.trapOverlaps;
    }

    self.keepPlayerOnGrid(player, body);
    

  });
};

CellController.prototype.applyPlayerRadiusToBody = function (player, playerbody, radius){

    var shape = playerbody.shapes[0];
    shape.radius = radius;
    playerbody.boundingRadius = shape.radius;

    shape.updateBoundingRadius();
    playerbody.updateBoundingRadius(); 

    player.radius = playerbody.boundingRadius;   

}

CellController.prototype.applyPlayerMassToBody = function (player, playerbody, mass){

    playerbody.mass = mass;
    playerbody.updateMassProperties();

    player.mass = playerbody.mass;
}


CellController.prototype.findPlayerOverlaps = function (playerIds, players, traps) {
  var self = this;

  var playerTree = new rbush();
  var hitAreaList = [];

  playerIds.forEach(function (playerId) {
    var player = players[playerId];
    player.hitArea = self.generateHitArea(player);
    hitAreaList.push(player.hitArea);
  });

  playerTree.load(hitAreaList);

  // playerIds.forEach(function (playerId) {
  //   var player = players[playerId];
  //   playerTree.remove(player.hitArea);
  //   var hitList = playerTree.search(player.hitArea);
  //   playerTree.insert(player.hitArea);

  //   hitList.forEach(function (hit) {
  //     if (!player.playerOverlaps) {
  //       player.playerOverlaps = [];
  //     }
  //     player.playerOverlaps.push(hit.target);
  //   });
  // });

  var trapIds = Object.keys(traps);
  trapIds.forEach(function (trapid) {
    var trap = traps[trapid];
    var trapHitArea = self.generateHitArea(trap, config.DEFAULT_TRAP_RANGE);
    var hitList = playerTree.search(trapHitArea);

    if (hitList.length) {
      // If multiple players hit the trap
      var randomIndex = Math.floor(Math.random() * hitList.length);
      var trappedTarget = hitList[randomIndex].target;

      if (!trappedTarget.trapOverlaps) {
        trappedTarget.trapOverlaps = [];
      }
      trappedTarget.trapOverlaps.push(trap);
    }
  });

  playerIds.forEach(function (playerId) {
    delete players[playerId].hitArea;
  });
};

CellController.prototype.generateHitArea = function (target, radius) {
  var targetRadius = radius || target.r || Math.round(target.radius / 2);
  return {
    target: target,
    minX: target.x - targetRadius,
    minY: target.y - targetRadius,
    maxX: target.x + targetRadius,
    maxY: target.y + targetRadius
  };
};

//Don't need this
CellController.prototype.testCircleCollision = function (a, b) {
  var radiusA = a.r || Math.round(a.radius / 2);
  var radiusB = b.r || Math.round(b.radius / 2);

  var circleA = new SAT.Circle(new SAT.Vector(a.x, a.y), radiusA);
  var circleB = new SAT.Circle(new SAT.Vector(b.x, b.y), radiusB);

  var response = new SAT.Response();
  var collided = SAT.testCircleCircle(circleA, circleB, response);

  return {
    collided: collided,
    overlapV: response.overlapV
  };
};


//Clear traps after fixed amount of time
CellController.prototype.clearTrapsTimeout = function(trapIds, traps, timeout){
  var trap_timeout = timeout || config.DEFAULT_TRAP_TIMEOUT;
  var now = Date.now();
  trapIds.forEach(function(trapid){
    if (now - traps[trapid].set_time > trap_timeout){
      //remove trap
      self.trapManager.removeTrap(trapid);
    }
  });
}

module.exports = CellController;