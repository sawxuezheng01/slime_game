var playState = {
	create: function(){
		// setInterval(function(){
		// 	console.log(Object.keys(users));
		// }, 3000);
		game.world.setBounds(0, 0, WORLD_WIDTH, WORLD_HEIGHT);
	    //set up bounds and start the game time
		game.time.advancedTiming = true;

	    //draw backgrounds
	    background = game.add.tileSprite(0, 0, WORLD_WIDTH, WORLD_HEIGHT, 'background');
	    drawRing();   
	    renderSkillSprite();
		game.stage.disableVisibilityChange = true;

		userAlive = true;
		player = {};
		users = {};

	    coins = {};
	    traps = {};

	    gameOverProcessed = false;
	    ranklist = [];
	    

	    if (RANK_TEXT){

		    //set up ranking box
		    rbox = game.add.sprite(game.canvas.width*6/7,0, 'rankbox');
		    rbox.alpha = 0.5;
		    rbox.width = game.canvas.width/7;
		    rbox.height = game.canvas.height/4;
		    
	        
		    ranktext = game.add.text(rbox.x + 10, rbox.y + 10, "", {
	        font: "22px Arial",
	        fill: "#000000",
	    	});

		    ranktext.fixedToCamera = true;
		    rbox.fixedToCamera = true;

		    //prevent them from being scaled with the camera
		    rbox.setScaleMinMax(1,1,1,1);
		    ranktext.setScaleMinMax(1,1,1,1);


		    rankChannel = socket.subscribe('rank-data');
		    rankChannel.watch(function(topten){
		    	ranklist = topten;
		    });
		}

        //Prepare player inputs
        game.input.mousePointer.leftButton.onUp.add(onPointerRelease,this);
        keys = {
        	w: game.input.keyboard.addKey(Phaser.Keyboard.W),
        	q: game.input.keyboard.addKey(Phaser.Keyboard.Q),
        	e: game.input.keyboard.addKey(Phaser.Keyboard.E),
        	s: game.input.keyboard.addKey(Phaser.Keyboard.S)
        };

		// if (STALKER){
		// 	camera = new Camera({});
		// 	camera.zoomIn(1.5);
		// }
		
		// game.renderer.renderSession.roundPixels = true;

		// setInterval(function(){
		// 	var playerOp = {};
		// 	var p = game.input.mousePointer; 
		// 	//angle between player object and the mouse, used to find direction
		// 	var angle = game.math.angleBetween(player.x * game.camera.scale.x, player.y * game.camera.scale.y,game.camera.x+p.x,game.camera.y+p.y);
		// 	playerOp.angle = angle;	

		// 	socket.emit('direction', playerOp);
		// }, 500);	

	},

	update: function(){

		self = this;
		var playerOp = {};
		var didAction = false;


		if (player.radius <= 20){
			camera_zoom_scale = 1.5;
			self.cond_a = true;
			self.cond_b = false;
			self.cond_c = false;
			self.cond_d = false;
		} else if (player.radius > 20 && player.radius <= 70){
			camera_zoom_scale = 1.25;	
			self.cond_b = true;
			self.cond_c = false;
			self.cond_d = false;
			self.cond_a = false;

		} else if (player.radius > 70 && player.radius <= 120){
			camera_zoom_scale = 0.75;	
			self.cond_c = true;
			self.cond_d = false;
			self.cond_b = false;
			self.cond_a = false;
		} else {
			camera_zoom_scale = 0.5;
			self.cond_d = true;
			self.cond_c = false;
			self.cond_b = false;
			self.cond_a = false;			
		}

		

		
		//oneshot the zoom_en, logic derived from k-map
		zoom_en = (self.cond_a && !self.cond_b && !self.cond_c && !self.cond_d && !self.cond_a_lastcycle) ||
					(!self.cond_a && self.cond_b && !self.cond_c && !self.cond_d && !self.cond_b_lastcycle) ||
					(!self.cond_a && !self.cond_b && self.cond_c && !self.cond_d && !self.cond_c_lastcycle) ||
					(!self.cond_a && !self.cond_b && !self.cond_c && self.cond_d && !self.cond_d_lastcycle);

		self.cond_a_lastcycle = self.cond_a;
		self.cond_b_lastcycle = self.cond_b;
		self.cond_c_lastcycle = self.cond_c;
		self.cond_d_lastcycle = self.cond_d;

		if (zoom_en) {
			if (STALKER){
				camera.zoomIn(camera_zoom_scale,500);
			} else {
				zoomIn(camera_zoom_scale,500);
			}
		}		

		//if player is instantiated
		if (Object.keys(player).length){

			if (player.alive != undefined){
				userAlive = player.alive;
			}
			
			
		}
		//if button is held down
		if (game.input.mousePointer.leftButton.isDown){
			var dur = game.input.mousePointer.leftButton.duration;

			var accel = dur*player.speed*player.durscaling;
			var percentage = accel/player.maxAcceleration * 100;

			//console.log(player.maxAcceleration);
			if (player.energybar){      
				//console.log("setting percentage")      
				player.energybar.setPercent(percentage);
			}

		} else {
			if (player.energybar){ 
				player.energybar.setPercent(0);
			}
		}

		if (keys.w.isDown){
			playerOp.b = 1;
			didAction = true;
		}

		if (keys.q.isDown){
			playerOp.d = 1;
			didAction = true;
		}

		if (keys.e.isDown){
			playerOp.t = 1;
			didAction = true;
		}
		
		if (keys.s.isDown){
			playerOp.s = 1;
			didAction = true;
		}		

		if (!userAlive){
			
			function handleGameOver(){
				gameOverProcessed = true;
				setTimeout(self.Win, 1500);
			}

			if (!gameOverProcessed){
				handleGameOver(); 
			}        
		}


		if (didCharge){
			didCharge = false;
			Object.assign(playerOp,chargeOp);
			didAction = true;
		}

		if (didAction && Date.now() - lastActionTime >= USER_INPUT_INTERVAL) {
			lastActionTime = Date.now();
			// Send the player operations for the server to process.
			// console.log("emit")
			socket.emit('action', playerOp);
		}

		game.world.bringToTop(skillspritegroup);
	},

    render: function() {
    		
	      var now = Date.now();

	      if (ENVIRONMENT == 'dev') {
	         // game.debug.text('FPS:   ' + game.time.fps, 2, 14, "#000000");

	        // game.debug.text('Camera pos: ' + [game.camera.x,game.camera.y], 2, 46, "#00FF00");
	        // game.debug.text('Player pos: ' + [player.x,player.y], 2, 60, "#00FF00");
	        // game.debug.text('Mouse pos: ' + [game.input.mousePointer.x,game.input.mousePointer.y], 2, 74, "#00FF00");
	        // game.debug.text('Click dur: ' + game.input.mousePointer.duration, 2, 90, "#00FF00");

	        if (player) {
	           game.debug.text('Score: ' + player.score, 2, 30, "#000000");
	           // game.debug.text("Press 'w' to become bigger. Left click to dash!", 2, game.canvas.height - 30, "#000000");
	        }
	      }

	      for (var i in users) {
	        if (users.hasOwnProperty(i)) {
	          var curUser = users[i];
	          if (now - curUser.clientProcessed > PLAYER_INACTIVITY_TIMEOUT) {
	            removeUser(curUser);
	          }
	        }
	      }

	      for (var j in coins) {
	        if (coins.hasOwnProperty(j)) {
	          var curCoin = coins[j];
	          if (now - curCoin.clientProcessed > COIN_INACTIVITY_TIMEOUT) {
	            removeCoin(curCoin);
	          }
	        }
	      }

	      var t = "";
	      for (var i in ranklist){
	      	var str =  parseInt(i) + 1 + ') ' + ranklist[i].name + ": \t" + ranklist[i].score + '\n';
	      	t += str;
	      }

	      if (RANK_TEXT){
	      	ranktext.setText(t);	
	      }
	      
    },

	Win: function(){
		game.state.start('win');
	}



};

function removeAllUserSprites() {
	
	for (var i in users) {
		if (users.hasOwnProperty(i)) {
			removeUser(users[i]);
		}
	}
}

function handleCellData(stateList) {

	
	stateList.forEach(function (state) {
		if (state.type == 'player') {
			updateUser(state);
		} else if (state.type == 'coin') {
			if (state.delete) {
				removeCoin(state);
			} else {
				renderCoin(state);
			}
		} else if (state.type == 'trap'){
			if (state.delete){
				removeTrap(state);
			} else {
				renderTrap(state);	
			}
		}
	});
	updatePlayerZIndexes();
}

var watchingCells = {};

/*
Data channels within our game are divided a grids and we only watch the cells
which are within our player's line of sight.
As the player moves around the game world, we need to keep updating the cell subscriptions.
*/
function updateCellWatchers(playerData, channelName, handler) {
	var options = {
		lineOfSight: PLAYER_LINE_OF_SIGHT
	};
	channelGrid.updateCellWatchers(playerData, channelName, options, handler);
}

function updateUserGraphics(user) {

	user.sprite.x = user.x;
	user.sprite.y = user.y;
	// var tweenposvar = {
	// 	x: user.x,
	// 	y: user.y
	// };

	// if (user.sprite.x){
	// 	var tweenposition = game.add.tween(user.sprite);
	// 	tweenposition.to(tweenposvar, 20, Phaser.Easing.Linear.None).start();	
	// } else {
	// 	user.sprite.x = user.x;
	// 	user.sprite.y = user.y;
	// }
	

	// var tween = game.add.tween(user.sprite);
	// tween.to( { width: user.radius*2, height: user.radius*2 }, 100, Phaser.Easing.Linear.None);
	// tween.start();	
	if (!user.wassupersize && user.issupersize){
		//play dig animation
		var tween = game.add.tween(user.sprite);
		tween.to({width: user.sprite.width*1.3, height: user.sprite.height*1.3}, 100, Phaser.Easing.Linear.None).start();

	} else if (user.wassupersize && !user.issupersize){
		//play undig animation
		var tween = game.add.tween(user.sprite);
		tween.to({width: user.sprite.width/1.3, height: user.sprite.width/1.3}, 100, Phaser.Easing.Linear.None).start();
	}

	if (user.radius != user.newradius){
		//play dig animation
		var tween = game.add.tween(user.sprite);
		tween.to({width: user.newradius*2, height: user.newradius*2}, 100, Phaser.Easing.Linear.None).start();

	} 

	//if user is snared 
	if (user.snared){
		//check if there is already a textlabel for snared
		if (user.snared_text){
			//if there is just do nothing
		} else { //else made a new snared_text
			user.snared_text = game.add.text(0, -15, "Snared !", {
				font: '14px Arial',
			});
			user.snared_text.anchor.setTo(0.5,0.5);
			user.sprite.addChild(user.snared_text);
		}
	} else { //if the user is not snared
		//but has a textlabel for it
		if (user.snared_text){ //delete it
			user.sprite.removeChild(user.snared_text);
			user.snared_text.destroy();
			delete user.snared_text;
		}
	}


	//DIG ANIMATIONS
	if (!user.wasdig && user.isdig){
		//play dig animation
		var tween = game.add.tween(user.sprite);
		tween.to({alpha: 0.3}, 50, Phaser.Easing.Linear.None).start();

	} else if (user.wasdig && !user.isdig){
		//play undig animation
		var tween = game.add.tween(user.sprite);
		tween.to({alpha: 1}, 50, Phaser.Easing.Linear.None).start();
	}

	//Handle rendering the crown
	if (ranklist && ranklist.length > 0){
		if (user.id == ranklist[0].id){

			if (!user.crown){
				user.crown = game.add.sprite(null,null,'clown-crown');
				user.crown.scale.x = 0.05;
				user.crown.scale.y = 0.05;
				user.sprite.addChild(user.crown);
				user.crown.x = -10;
				user.crown.y = -23;

				

				user.wasking = true;
			}
		} else {
			//if user was rank 1 but not anymore
			//delete the crown
			if (user.wasking){
				user.sprite.removeChild(user.crown);
				user.crown.destroy();
				delete user.crown;

				user.wasking = false;
			}
		}
	}

	if (user.energybar){
		user.energybar.setPosition(user.x, user.y + 5 + user.sprite.height/2);
	}

	if (!user.direction) {
		user.direction = 'down';
	}

	if (!user.alive){

		var tween = game.add.tween(user.sprite);
		tween.to( { alpha: 0 }, 300, Phaser.Easing.Linear.None);
		tween.start();

		//kill the energy bar if the user has one
		if (user.energybar){
			user.energybar.kill();
		}

		user.label.destroy();

	}
	
	switch (user.direction){
		case 'left': 
			user.sprite.frame = 0; 
			break;
		case 'down':
			user.sprite.frame = 1; 
			break;
		case 'right': 
			user.sprite.frame = 2;
			break;
		case 'up' : 
			user.sprite.frame = 3; 
			break;
		case 'default':
			user.sprite.frame = 1;
			break;
	}
	//user.sprite.animations.play(user.direction);

	// user.label.alignTo(user.sprite, Phaser.BOTTOM_CENTER, 0, 5);

}

function moveUser(userId, x, y) {
	var user = users[userId];
	user.x = x;
	user.y = y;
	updateUserGraphics(user);
	user.clientProcessed = Date.now();

	if (user.id == playerId) {
		updateCellWatchers(user, 'cell-data', handleCellData);
	}
}

function removeUser(userData) {
	var user = users[userData.id];
	if (user) {
		user.sprite.destroy();
		user.label.destroy();
		delete users[userData.id];
	}
}

function createTexturedSprite(options) {
	var sprite = game.add.sprite(0, 0, options.texture);
	sprite.anchor.setTo(0.5, 0.5);

	return sprite;
}

function createUserSprite(userData) {
	var user = {};
	users[userData.id] = user;
	user.id = userData.id;
	user.swid = userData.swid;
	user.name = userData.name;
	user.slime_texture = userData.slime_texture;

	var textStyle = {
		font: '100px Arial',
		fill: '#000000',
		align: 'center',
		fontWidth: 'bold'
	};

	user.label = game.add.text(0, 0, user.name, textStyle);
	user.label.anchor.set(0.5);
	user.label.scale.x = 0.2;
	user.label.scale.y = 0.2;

	var sprite;

	if (userData.id == playerId) {
		sprite = createTexturedSprite({
			texture: slime_texture_list[user.slime_texture]
		});
		users[playerId].energybar = new HealthBar(game, {x: users[playerId].x, y: users[playerId].y + 30, width: 40, height: 5, animationDuration: 10});          
		//user.texturePrefix = 'you';
	} else if (userData.subtype == 'bot') {
		sprite = createTexturedSprite({
			texture: slime_texture_list[user.slime_texture]
		});
		user.texturePrefix = 'bot';
	} else {
		sprite = createTexturedSprite({
			texture: slime_texture_list[user.slime_texture]
	});
	//user.texturePrefix = 'others';
	}

	user.score = userData.score;
	user.sprite = sprite;

	user.sprite.addChild(user.label);
	user.label.y = 30;

	// if (user.energybar){
	// 	user.sprite.addChild(user.energybar.bgSprite);
	// 	user.sprite.addChild(user.energybar.barSprite);
		
	// 	user.energybar.barSprite.x = -20;
	// 	user.energybar.barSprite.y = 17;
	// 	user.energybar.bgSprite.y = 17;
		
	// }
	//add animations from spritesheet
	// user.sprite.animations.add('left', [0],10,true);
	// user.sprite.animations.add('down', [1],10,true);
	// user.sprite.animations.add('right', [2],10,true);
	// user.sprite.animations.add('up', [3],10,true);

	user.sprite.width = userData.radius*2;
	user.sprite.height = userData.radius*2;
	user.diam = user.sprite.width;

	user.alive = userData.alive;
	moveUser(userData.id, userData.x, userData.y);
	if (userData.id == playerId) {
        player = user;
        
        game.camera.setSize(window.innerWidth, window.innerHeight);
        game.camera.follow(user.sprite);

	}
}

function updatePlayerZIndexes() {
	var usersArray = [];
	for (var i in users) {
		if (users.hasOwnProperty(i)) {
			usersArray.push(users[i]);
		}
	}
	usersArray.sort(function (a, b) {
		if (a.y < b.y) {
			return -1;
		}
		if (a.y > b.y) {
			return 1;
		}
		return 0;
	});
	usersArray.forEach(function (user) {
		user.label.bringToTop();
		user.sprite.bringToTop();

	});
}


function updateUser(userData) {
	var user = users[userData.id];

	if (user) {
		user.maxAcceleration = userData.maxAcceleration;
		user.speed = userData.speed;
		user.score = userData.score;
		user.direction = userData.direction;
		user.alive = userData.alive;
		user.durscaling = userData.durscaling;

		user.radius = user.newradius;
		user.newradius = userData.radius;
		user.snared = userData.snared;

		user.wasdig = user.isdig;
		user.isdig = userData.dig;
		user.wassupersize = user.issupersize;	
		user.issupersize = userData.supersize;

		moveUser(userData.id, userData.x, userData.y);

	} else {
		//create the sprite data
		// if (userData.alive){
			createUserSprite(userData);	
		// }
	}  
}

function removeCoin(coinData) {
	var coinToRemove = coins[coinData.id];
	if (coinToRemove) {
		coinToRemove.sprite.destroy();
		delete coins[coinToRemove.id];
	}
}

function removeTrap(trapData) {
	var trapToRemove = traps[trapData.id];
	if (trapToRemove) {
		trapToRemove.sprite.destroy();
		delete traps[trapData.id];
	}
}

function renderCoin(coinData) {
	if (coins[coinData.id]) {
		var coin = coins[coinData.id];
		coin.sprite.x = coinData.x;
		coin.sprite.y = coinData.y;
		coins[coinData.id].clientProcessed = Date.now();
	} else {
		var coin = coinData;
		coins[coinData.id] = coin;
		var color_list = ['r','g','b','y'];
		var rand_color = color_list[Math.floor(Math.random()*color_list.length)];

		coin.sprite = createTexturedSprite({
			// texture: 'diamond-' + (coinData.t || '1') + '-' + rand_color
			// texture: 'diamond-' + (coinData.t || '1') + '-' + 'b'
			texture: 'stone'
		});
		coin.sprite.x = coinData.x;
		coin.sprite.y = coinData.y;
		coin.sprite.width = coinData.r*2;
		coin.sprite.height = coinData.r*2;
		coin.clientProcessed = Date.now();
	}
}

function renderTrap(trapData){
	if (traps[trapData.id]){
		var trap = traps[trapData.id];
		traps[trapData.id].clientProcessed = Date.now();
	} else {
		var trap = trapData;
		traps[trapData.id] = trap;
		trap.sprite = createTexturedSprite({
			texture: 'trap'
		});
		trap.sprite.x = trapData.x;
		trap.sprite.y = trapData.y;
		trap.sprite.width = trapData.r*2;
		trap.sprite.height = trapData.r*2;
		trap.clientProcessed = Date.now();
	}
}

function drawRing(){
	var graphics = game.add.graphics(0, 0);

	RING_BOUNDARIES.forEach(function(circle){
		graphics.lineStyle(5, 0xFFD900, 1);
		graphics.beginFill(circle.color, 1);

		//Params: x, y, diameter
		graphics.drawCircle(circle.x, circle.y, circle.r*2);

	});          
}

var didCharge;
var chargeOp;

function onPointerRelease(pointer){
	chargeOp = {};
	//reference to mousePointer Object
	var p = game.input.mousePointer; 

	//angle between player object and the mouse, used to find direction
	var angle = game.math.angleBetween(player.x * game.camera.scale.x, player.y * game.camera.scale.y,game.camera.x+p.x,game.camera.y+p.y);

	
	
	//duration of pointer held
	var dur = pointer.timeUp - pointer.timeDown;

	chargeOp.c = 1;
	chargeOp.c_dur = dur;
	chargeOp.c_angle = angle;
	didCharge = true;
}        

function zoomIn(scale, duration, extraObjects){

		if (duration){
			var tween = game.add.tween(game.camera.scale);
			tween.to({x: scale, y: scale}, duration, Phaser.Easing.Linear.None, true);	
			// game.camera.lerp.x = 0.01*scale;
			// game.camera.lerp.y = 0.01*scale;

			if (extraObjects){
				extraObjects.forEach(function(object){

					var tween = game.add.tween(object.scale);
					tween.to({x: scale, y: scale}, duration, Phaser.Easing.Linear.None, true);
				})
			}

		} else {
			game.camera.scale.x = scale;
			game.camera.scale.y = scale;

			if (extraObjects){
				extraObjects.forEach(function(object){
					object.scale.x = scale;
					object.scale.y = scale;
				})
			}
		}

	}

function renderSkillSprite(){

	var vertical_offset = 550;
	var horizontal_offset = 30;

	skillspritegroup = new Phaser.Group(game);
	
	//create sprite for skills
	var invisible_skillsprite = game.add.sprite(horizontal_offset + 0, vertical_offset, "skill-invisible");
	var supersize_skillsprite = game.add.sprite(horizontal_offset + 64, vertical_offset, "skill-supersize");
	var teleport_skillsprite = game.add.sprite(horizontal_offset + 64*2, vertical_offset, "skill-teleport");
	var trap_skillsprite = game.add.sprite(horizontal_offset + 64*3, vertical_offset, "skill-trap");

	//create labels for each of them
	spritelabels = [game.add.text(horizontal_offset + 5, vertical_offset + 5, 'Q', {font: "50px Arial", fontWidth: 'bold'}),
					game.add.text(horizontal_offset + 5 + 64, vertical_offset + 5, 'W', {font: "50px Arial", fontWidth: 'bold'}),
					game.add.text(horizontal_offset + 5 + 64*2, vertical_offset + 5, 'E', {font: "50px Arial", fontWidth: 'bold'}),
					game.add.text(horizontal_offset + 5 + 64*3, vertical_offset + 5, 'S', {font: "50px Arial", fontWidth: 'bold'})];

	spritelabels.forEach(function(label){
		label.setScaleMinMax(0.5,0.5,0.5,0.5);
	})

	var spritegroup = [invisible_skillsprite, supersize_skillsprite, teleport_skillsprite, trap_skillsprite];
	var skillnamelist = ["Phase-shift" , "Supersize", "Teleport", "Ankle Snare - 10 seconds recharge time."];
	
	spritegroup.forEach(function(sprite, idx){
		sprite.width = 64;
		sprite.height = 64;
		sprite.inputEnabled = true;

		//create an event listener on mouse oversprite
		sprite.events.onInputOver.add(function(){
			if (!sprite.description) {
				sprite.description = game.add.text(0,-10, skillnamelist[idx], {font: "50px Arial", fontWidth: 'bold'});
				sprite.description.setScaleMinMax(0.5,0.5,0.5,0.5);
				sprite.addChild(sprite.description);
			} 
		})

		sprite.events.onInputOut.add(function(){
			if (sprite.description) {
				sprite.removeChild(sprite.description);
				delete sprite.description;
			} 
		})
	});




	skillspritegroup.addMultiple(spritegroup);
	skillspritegroup.addMultiple(spritelabels);
	skillspritegroup.fixedToCamera = true;
	
}