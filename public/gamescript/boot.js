

var bootState = {
  create: function(){

    //set up socket
    socket = socketCluster.connect({
        codecEngine: scCodecMinBin,
        autoReconnect: false
    });
    //set up channelGrid for client-server data transfer
    socket.emit('getWorldInfo', null, function (err, data) {

      //callback function
      WORLD_WIDTH = data.width;
      WORLD_HEIGHT = data.height;
      WORLD_COLS = data.cols;
      WORLD_ROWS = data.rows;
      WORLD_CELL_WIDTH = data.cellWidth;
      WORLD_CELL_HEIGHT = data.cellHeight;
      WORLD_CELL_OVERLAP_DISTANCE = data.cellOverlapDistance;
      SERVER_WORKER_ID = data.serverWorkerId;
      ENVIRONMENT = data.environment;
      RING_BOUNDARIES = data.ringboundaries;

      channelGrid = new ChannelGrid({
        worldWidth: WORLD_WIDTH,
        worldHeight: WORLD_HEIGHT,
        rows: WORLD_ROWS,
        cols: WORLD_COLS,
        cellOverlapDistance: WORLD_CELL_OVERLAP_DISTANCE,
        exchange: socket
      });

      game.state.start('load');
    });
  }
}