

//Menu interface of the game
var menuState = {

	create: function(){
    game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
    menu_background = game.add.sprite(0, 0, 'menu');

    menu_background.height = 2000;
    menu_background.width = 2000;
    menu_background.setScaleMinMax(3,3,3,3);

    var menu_x = game.width/2 - 120;
    var menu_y = game.height/2 + 5;
    var desc_x = game.width/2 - 150;
    var desc_y = game.height/2 + 160;

    var menu_img = game.add.sprite(game.width*2/5, game.height/13, 'kingslime');

    //define a modulo function that works for all range of integers
    Number.prototype.mod = function(n){
      return (this%n + n)%n;
    }

    this.skinidx = 0;
    
    self = this;
    function arrow_right_clicked(){
      self.skinidx++;
      self.skinidx = (self.skinidx.mod(Object.keys(slime_texture_list).length));
      self.skin_select.destroy();
      self.skin_select = renderSkin(self.skinidx + 1, desc_x + 160, desc_y + 110);
    }

    function arrow_left_clicked(){
      self.skinidx--;
      self.skinidx = (self.skinidx.mod(Object.keys(slime_texture_list).length));
      self.skin_select.destroy();
      self.skin_select = renderSkin(self.skinidx + 1, desc_x + 160, desc_y + 110);
    }

    var arrow_right = game.add.button(desc_x + 225, desc_y + 100, 'arrow-right', arrow_right_clicked, this);
    var arrow_left = game.add.button(desc_x + 75, desc_y + 100, 'arrow-left', arrow_left_clicked, this);

    this.skin_select = renderSkin(this.skinidx + 1, desc_x + 160, desc_y + 110);

    //var input = game.add.inputField(window.innerWidth/2, window.innerHeight/2);
    var label = game.add.text(menu_x, menu_y, "Nickname : ", {
        font: '20px Arial',
        fill: '#00000',
        align: 'center',
        fontWeight: 'bold'
    });

    var description = game.add.text(desc_x, desc_y, "           Push! Grow! And dominate!", {
        font: '60px Arial',
        fill: '#00000',
        align: 'center',
        fontWeight: 'bold'
    });

    description.scale.x = 0.3;
    description.scale.y = 0.3;


    playerName = game.add.inputField(menu_x + 120, menu_y -5, {
        font: '18px Arial',
        fill: '#00000',
        fillAlpha: 1,
        fontWeight: 'bold',
        width: 150,
        max: 20,
        padding: 8,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 6,
        placeHolder: 'name',
        textAlign: 'center',
        zoom: true
    });

  function actionOnClick(event){
    this.start();
  }

  var button = game.add.button(menu_x + 65, menu_y + 50, 'play_button', actionOnClick, this);
      socket.on('disconnect', removeAllUserSprites);
      // socket.on('connect', joinWorld);
	},

  start: function(){
    if (socket.state == 'open') {
        joinWorld();   
    } else {
      socket.connect();
      joinWorld();
    }
  }
};

//Join World
function joinWorld() {
  socket.emit('join', {
  name: playerName.value,
  slime_texture: 1 + menuState.skinidx
  }, function (err, playerData) {

  playerId = playerData.id;
  
  updateCellWatchers(playerData, 'cell-data', handleCellData);

  //start the game
  game.state.start('play');
  });
}


function renderSkin(idx, pos_x, pos_y){


  if (slime_texture_list[idx]){
    var skinsprite = game.add.sprite(pos_x || 0 ,pos_y || 0 , slime_texture_list[idx]);
    skinsprite.anchor.setTo(0.5, 0.5);
    skinsprite.scale.x = 1.5;
    skinsprite.scale.y = 1.5;
    
    return skinsprite;    
  } else {
     console.log("index " + idx + " : " + slime_texture_list[idx])
    throw "slime texture index out of range";
  }

}