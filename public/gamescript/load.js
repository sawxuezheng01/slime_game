
var loadState = {
	preload: function(){

          //preloads the spritesheets
          game.load.spritesheet('play_button', 'assets/play_button.png', 150, 75);
          game.load.image('clown-crown', 'assets/crown.png');
          game.load.image('kingslime', 'assets/menu_img.png');
          game.load.image('trap', 'assets/trap2.png');

          game.load.image('skill-teleport', 'assets/skill_teleport.png');
          game.load.image('skill-invisible', 'assets/skill_invisible.png');
          game.load.image('skill-trap', 'assets/skill_trap.png');
          game.load.image('skill-supersize', 'assets/skill_supersize.png');

          game.load.image('arrow-right', 'assets/arrow_right.png');
          game.load.image('arrow-left', 'assets/arrow_left.png');

          game.load.spritesheet(slime_texture_list[1], slimeTextures[1], 26, 26, 4);
          game.load.spritesheet(slime_texture_list[2], slimeTextures[2], 26, 26, 4);
          game.load.spritesheet(slime_texture_list[3], slimeTextures[3], 26, 26, 4);
          game.load.spritesheet(slime_texture_list[4], slimeTextures[4], 26, 26, 4);
          game.load.spritesheet(slime_texture_list[5], slimeTextures[5], 26, 26, 4);

          game.load.image('stone', 'assets/stone.png');

          game.load.image('background', BACKGROUND_TEXTURE);
          game.load.image('menu', MENU_IMAGE);
          game.load.image('rankbox', RANK_BOX);
	},

	create: function(){
		game.state.start('menu');

	}
}

