
var socket = {};

//Global variables to store game information
var STALKER = false;
var RANK_TEXT = true;

var lastActionTime = 0;
var game, playerId, playerName;
var userAlive = true;
var player = {};
var gameOverProcessed = false;
var users = {};
var coins = {};
var traps = {};

var channelGrid = {};

var WORLD_WIDTH;
var WORLD_HEIGHT;
var WORLD_COLS;
var WORLD_ROWS;
var WORLD_CELL_WIDTH;
var WORLD_CELL_HEIGHT;
var PLAYER_LINE_OF_SIGHT = Math.round(window.innerWidth);
var PLAYER_INACTIVITY_TIMEOUT = 700;
var USER_INPUT_INTERVAL = 20;
var COIN_INACTIVITY_TIMEOUT = 2200;
var ENVIRONMENT;
var SERVER_WORKER_ID;

var cameraFollow = false;

N_SLIME_TEXTURE = 5;

// Map the score value to the texture.
var diamondTextures = {
  1: 'assets/diamond-1-r.gif',
  2: 'assets/diamond-2-r.gif',
  3: 'assets/diamond-3-r.gif',
  4: 'assets/diamond-4-r.gif',
  5: 'assets/diamond-1-g.gif',
  6: 'assets/diamond-2-g.gif',
  7: 'assets/diamond-3-g.gif',
  8: 'assets/diamond-4-g.gif',
  9: 'assets/diamond-1-b.gif',
  10: 'assets/diamond-2-b.gif',
  11: 'assets/diamond-3-b.gif',
  12: 'assets/diamond-4-b.gif',
  13: 'assets/diamond-1-y.gif',
  14: 'assets/diamond-2-y.gif',
  15: 'assets/diamond-3-y.gif',
  16: 'assets/diamond-4-y.gif'
};

var slimeTextures = {
  1: 'assets/slime_red.png',
  2: 'assets/slime_black.png',
  3: 'assets/slime_green.png',
  4: 'assets/slime_purple.png',
  5: 'assets/slime_white.png',
};

var slime_texture_list = {
  1: 'red-slime',
  2: 'black-slime',
  3: 'green-slime',
  4: 'purple-slime',
  5: 'white-slime'
};

// 1 means no smoothing. 0.1 is quite smooth.
var CAMERA_SMOOTHING = 0.8;
var BACKGROUND_TEXTURE = 'assets/background-texture-test.png';
var MENU_IMAGE = 'assets/sky.png';
var RANK_BOX = 'assets/ranking_display_box.png';

window.onload = function () {
  game = new Phaser.Game('100', '100', Phaser.CANVAS, '');

  //Add plugins for game
  Phaser.Device.whenReady(function () {
      game.plugins.add(PhaserInput.Plugin);
  });

  //Add game states
  game.state.add("boot", bootState);
  game.state.add("load", loadState);
  game.state.add("menu", menuState);
  game.state.add("play", playState);
  game.state.add("win", winState);
  game.state.start('boot');  
}

// window.addEventListener("resize", resizeGameCanvas);

// game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
// function resizeGameCanvas(){
//   game.scale.setGameSize(window.innerWidth, window.innerHeight);

// }