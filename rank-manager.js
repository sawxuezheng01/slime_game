//This object keep track of the players who is in top ten
var RankManager = function(){
	var self = this;
	this.playerlist = [];
	this.players = {};
}

//Should only be called when player joins the server
//to maintain 1 copy of the player
RankManager.prototype.addPlayerToRanking = function(player){
	
	if (player){
		this.playerlist.push(player.id);
		this.players[player.id] = player;
	}
}


//Remove the player from the instance's list
RankManager.prototype.removePlayerFromRanking = function(player){
	
	if (player){
		//get the index
		var idx = this.playerlist.indexOf(player.id);
		
		if (idx > -1) {
			//remove the playerid from list
    		this.playerlist.splice(idx, 1);
		}
		//delete from the player list
		delete this.players[player.id];
	}	
}


//Update the data stored in the lists
RankManager.prototype.updatePlayerScore = function(playerid, score, name){
	if (this.players[playerid]){
		this.players[playerid].score = score;
		this.players[playerid].name = name;
	}
}

//Return the Top-N-List in the ranking
RankManager.prototype.getTopN = function(number){
	self = this;

	if (this.playerlist.length == 0){
		return [];
	} else {
		this.playerlist.sort(function(a,b){
		return self.players[b].score - self.players[a].score;
	});

		var clone = this.playerlist.slice(0);
		var toptenidlist = clone.splice(0,number);
		var rankerlist = [];

		toptenidlist.forEach(function(id){
			rankerlist.push({name: self.players[id].name, score: self.players[id].score, id: id});
		})

		return rankerlist;
	}
}

module.exports = RankManager;