
// Global configuration options for IOGrid back end

module.exports = {
  SIMULATION_STEP: 0.01,
  N_SLIME_TEXTURES: 5,
  // Having a large world (lower player density) is more efficient.
  // You can divide it up into cells to split up the workload between
  // multiple CPU cores.
  WORLD_WIDTH: 4000,
  WORLD_HEIGHT: 4000,
  // Dividing the world into tall vertical strips (instead of square cells)
  // tends to be more efficient (but this may vary depending on your use case
  // and world size).
  WORLD_CELL_WIDTH: 1000,
  WORLD_CELL_HEIGHT: 4000,
  /*
    The WORLD_CELL_OVERLAP_DISTANCE allows players/states from two different
    cells on the grid to interact with one another.
    States from different cells will show up in your cell controller but will have a
    special 'external' property set to true.
    This represents the maximum distance that two states can be from one another if they
    are in different cells and need to interact with one another.
    A smaller value is more efficient. Since this overlap area requires coordination
    between multiple cells.
  */
  WORLD_CELL_OVERLAP_DISTANCE: 150,
  /*
    This is the interval (in milliseconds) within which the world updates itself.
    It also determines the frequency at which data is broadcast to users.
    Making this value higher will boost performance and reduce bandwidth consumption
    but will increase lag. 20ms is actually really fast - If you add some sort of
    motion smoothing on the front end, 50ms or higher should be more than adequate.
  */
  WORLD_UPDATE_INTERVAL: 20,
  // Delete states which have gone stale (not being updated anymore).
  WORLD_STALE_TIMEOUT: 1000,
  // Coins don't move, so we will only refresh them
  // once per second.
  SPECIAL_UPDATE_INTERVALS: {
    100: ['coin']
  },

  MATERIAL_RESTITUTION: 0.2,
  COLLIDE_TAG_INTERVAL: 10000,
  CONSTANT_FORCE_APPLIED: 8000,
  CROSS_SPEED: 1700,
  DEFAULT_TRAP_RANGE: 15,
  PLAYER_MAX_TRAPCOUNT: 2,
  DEFAULT_TRAP_TIMEOUT: 25000,
  TRAP_CHARGE_DURATION: 10000,

  //STATUS EFFECTS
  DEFAULT_SNARED_DURATION: 1500,

  PLAYER_DEFAULT_MOVE_SPEED: 4000,
  PLAYER_DEFAULT_MAX_ACCELERATION: 3000000,
  PLAYER_DEFAULT_DAMPING: 0.98,
  PLAYER_DEFAULT_RESTITUTION: 0.98,
  PLAYER_DIAMETER: 20,
  PLAYER_MASS: 20,
  PLAYER_BREAK_COOLDOWN_INTERVAL: 3000,
  PLAYER_CHARGE_COOLDOWN_INTERVAL: 0,
  PLAYER_TELE_COOLDOWN_INTERVAL: 2000,
  PLAYER_DIG_COOLDOWN_INTERVAL: 1500,
  PLAYER_TRAP_COOLDOWN_INTERVAL: 1000,
  PLAYER_DEFAULT_DURSCALING: 1,

  PLAYER_SNARE_DURATION: 2000,
  
  //should be in the order of 10000 
  PLAYER_MAX_ACCELERATION_SCALING: 300000,
  MAX_PLAYER_RADIUS: 75,

  SUPERSIZE_RADIUS_MULTIPLIER: 1.3,
  SUPERSIZE_MASS_MULTIPLIER: 2,
  SUPERSIZE_DAMPING: 0.995,
  SUPERSIZE_BULLY_MULTIPLER: 100000,
  SCORE_RADIUS_MULTIPLIER: 0.1,
  SCORE_MASS_MULTIPLIER: 0.1,

  // COLLISION_MULTIPLIER: 50000,
  COLLISION_MULTIPLIER: 20000,
  KILL_SCORE_MULTIPLIER: 0.5,

  NO_SPAWN_RADIUS: 20,
  // Note that the number of bots needs to be either 0 or a multiple of the number of
  // worker processes or else it will get rounded up/down.
  BOT_COUNT: 2,
  BOT_MOVE_SPEED: 1000,
  BOT_MASS: 20,
  BOT_DEFAULT_DIAMETER: 20,
  BOT_CHANGE_DIRECTION_PROBABILITY: 0.3,
  BOT_SUPERSIZE_PROBABILITY: 0.1,
  BOT_MAX_CHARGE_TIME: 80,

  RING_BOUNDARIES: [ {
    x: 1050,
    y: 2000,
    r: 350,
    color: 0xEED5B7
    },
     {
    x: 2950,
    y: 2000,
    r: 350,
    color: 0xEED5B7
    },
    {
    x: 2000,
    y: 2000,
    r: 600,
    color: 0xEED5B7
    },
  ],

  STONE_DEFAULT_DAMPING: 0.3,

  COIN_UPDATE_INTERVAL: 500,
  COIN_DROP_INTERVAL: 20,
  COIN_MAX_COUNT: 30,
  COIN_PLAYER_NO_DROP_RADIUS: 30,
  OUTER_RING_NOSPAWN_RADIUS: 25,
  // The probabilities need to add up to 1.
  COIN_TYPES: [
    {
      type: 5,
      value: 10,
      radius: 5,
      probability: 0.6
    },
    {
      type: 4,
      value: 300,
      radius: 15,
      probability: 0.25
    },
    {
      type: 3,
      value: 450,
      radius: 20,
      probability: 0.1
    },
    {
      type: 2,
      value: 700,
      radius: 50,
      probability: 0.03
    },
    {
      type: 1,
      value: 800,
      radius: 100,
      probability: 0.02
    }
  ],


  // We can use this to filter out properties which don't need to be sent
  // to the front end.
  OUTBOUND_STATE_TRANSFORMERS: {
    coin: genericStateTransformer,
    player: genericStateTransformer
  }
};

var privateProps = {
  ccid: true,
  tcid: true,
  mass: true,
  changeDirProb: true,
  supersizeProb: true,
  supersize: true,
  outofbound: true,
  repeatOp: true,
  swid: true,
  processed: true,
  pendingGroup: true,
  group: true,
  version: true,
  external: true,
  velocity: true,
  supersize: false,
  lastapplybreak: true,
  lastapplycharge: true,
  lastapplydig: true,
  lastapplytele: true,
  lastapplytrap: true,
  lastangle: true,
  lasthitby: true,
  tagged_time: true,
  collision_en: true,
  trap_count: false,
  snared: false,
  snared_time: true,
  snare_charge_timer: true,
  tele: false,
  dig: false,
  r: false
};

function genericStateTransformer(state) {
  var clone = {};
  Object.keys(state).forEach(function (key) {
    if (!privateProps[key]) {
      clone[key] = state[key];
    }
  });
  return clone;
}
