var uuid = require('uuid');
var SAT = require('sat');
var config = require('./config');
var botnames = require('./botnames');


var BOT_DEFAULT_DIAMETER = 80;
var BOT_DEFAULT_SPEED = 1;
var BOT_DEFAULT_MASS = 10;
var BOT_DEFAULT_CHANGE_DIRECTION_PROBABILITY = 0.01;
var BOT_DEFAULT_SUPERSIZE_PROBABILITY = 0.2;

var BotManager = function (options) {
  this.worldWidth = options.worldWidth;
  this.worldHeight = options.worldHeight;
  if (options.botMoveSpeed == null) {
    this.botMoveSpeed = BOT_DEFAULT_SPEED;
  } else {
    this.botMoveSpeed = options.botMoveSpeed;
  }
  this.botMass = options.botMass || BOT_DEFAULT_MASS;
  this.botChangeDirectionProbability = options.botChangeDirectionProbability || BOT_DEFAULT_CHANGE_DIRECTION_PROBABILITY;
  this.botSuperSizeProbabilty = options.botSuperSizeProbabilty || BOT_DEFAULT_SUPERSIZE_PROBABILITY;
  this.botDefaultDiameter = options.botDefaultDiameter || BOT_DEFAULT_DIAMETER;
  this.maxBotCount = config.BOT_COUNT;

  this.botList = {};
  // this.botMoves = [
  //   {u: 1},
  //   {d: 1},
  //   {r: 1},
  //   {l: 1}
  // ];
};

BotManager.prototype.generateRandomPosition = function() {
  var rand_circle = config.RING_BOUNDARIES[Math.floor(Math.random()*config.RING_BOUNDARIES.length)];

  var r = Math.random();
  var a = 2*Math.PI*Math.random();
  var offset_x = rand_circle.x;
  var offset_y = rand_circle.y;

  var rand_x = offset_x + r*rand_circle.r *Math.cos(a);
  var rand_y = offset_y + r*rand_circle.r *Math.sin(a);

  return {
    x: Math.round(rand_x),
    y: Math.round(rand_y)
  }
};


BotManager.prototype.generateRandomAvailablePosition = function(cellPlayers){
  var circles = [];
  var position;

  if (cellPlayers){
    players = cellPlayers;

    for (var i in players) {
      var curPlayer = players[i];
      circles.push(new SAT.Circle(new SAT.Vector(curPlayer.x, curPlayer.y), curPlayer.radius + config.NO_SPAWN_RADIUS));
    } 
  }

  //console.log(Object.keys(cellPlayers));
  // console.log(circles);
  var validPosition = true;
  var count = 0;

  // console.log("start infinite loop")
  while(true){
    //generate a random position and construct a point
    var tempPosition = this.generateRandomPosition();
    var tempPoint = new SAT.Vector(tempPosition.x, tempPosition.y);
    
    //check if it is a valid position ie not inside the circles
    for (var k = 0; k < circles.length; k++) {
      if (SAT.pointInCircle(tempPoint, circles[k])) {
        validPosition = false;
        // console.log(tempPoint)
        // console.log("setting valid position to false");
        break;
      }
    }

    //if it is valid then get out of loop
    //get out of loop anyways if already tried 5 times
    if (circles.length == 0 || validPosition || count > 5){
      position = tempPosition;
      break;
    }

    count++;
  }
  // console.log("end infinite loop")
  if (count > 5){
    return null;
  } else {
    return position;
  }
}

BotManager.prototype.addBot = function (options, cellPlayers) {
  if (!options) {
    options = {};
  }


  var diameter = options.diam || this.botDefaultDiameter;
  var radius = Math.round(diameter / 2);
  var botId = uuid.v4();

  var bot = {
    id: botId,
    type: 'player',
    subtype: 'bot',
    name: options.name || botnames.BOT_NAMES[Math.floor(Math.random()*botnames.BOT_NAMES.length)],
    // score: options.score || 0,
    score: Math.floor(Math.random()*20) + 30,
    speed: options.speed == null ? this.botMoveSpeed : options.speed,
    mass: options.mass || this.botMass,
    velocity: [0,0],
    radius: diameter/2,
    changeDirProb: this.botChangeDirectionProbability,
    supersizeProb: this.botSuperSizeProbabilty,
    maxAcceleration: config.PLAYER_DEFAULT_MAX_ACCELERATION,
    alive: 1,
    lastapplybreak: 0,
    lastapplycharge: 0,
    lastangle: 0,
    supersize: false,
    slime_texture: 1 + Math.floor(Math.random()*config.N_SLIME_TEXTURES),
    justspawned: true,
    op: {}
  };

  if (options.x && options.y) {
    bot.x = options.x;
    bot.y = options.y;
  } else {
    var position = this.generateRandomAvailablePosition(cellPlayers);
    //if not a valid position
    if (position == null){
      //return do not spawn a bot
      return null;
    }

    if (options.x) {
      bot.x = options.x;
    } else {
      bot.x = position.x;
    }
    if (options.y) {
      bot.y = options.y;
    } else {
      bot.y = position.y;
    }
  }

  // console.log(this.botList);
  //keep track of bots
  this.botList[bot.id] = bot; 
  return bot;
};

BotManager.prototype.removeBot = function (bot) {

  if (this.botList[bot.id]){
    delete this.botList[bot.id];
    bot.delete = 1; 
  }  
};

module.exports.BotManager = BotManager;
